#!/bin/bash

echo "Installing Prometheus node_exporter v0.18.1.linux-amd64"
echo "Extracting node_exporter ..."
echo "Please provide sudo password when asked."
sudo -v
sudo mkdir /opt/node_exporter/
sudo tar -C /opt/node_exporter/ -xvf node_exporter-0.18.1.linux-amd64.tar.gz 
echo "Installing systemd service ..."
sudo cp node_exporter.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable node_exporter
echo "Starting node_exporter ..." 
sudo systemctl start node_exporter
echo "All done. Node Exporter is running on http://localhost:9100/metrics"


