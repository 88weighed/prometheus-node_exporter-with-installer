#!/bin/bash

echo "Installing Prometheus node_exporter v0.18.1.linux-amd64"
echo "Extracting node_exporter ..."
mkdir /opt/node_exporter/
tar -C /opt/node_exporter/ -xvf node_exporter-0.18.1.linux-amd64.tar.gz 
echo "Installing systemd service ..."
cp node_exporter.service /etc/systemd/system
systemctl daemon-reload
systemctl enable node_exporter
echo "Starting node_exporter ..." 
systemctl start node_exporter
echo "All done. Node Exporter is running on http://localhost:9100/metrics"


