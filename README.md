# Prometheus node_exporter with installer

To install with sudo:
- Clone this repo
- Go into the directory
- ./ne_install.sh
- Provide your sudo password when asked

To install without sudo (ie. as root):
- Clone this repo
- Go into the directory
- ./nosude_ne_install.sh
